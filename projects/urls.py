from django.urls import path
from projects.views import list_projects, list_projects_detail, create_project

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", list_projects_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
